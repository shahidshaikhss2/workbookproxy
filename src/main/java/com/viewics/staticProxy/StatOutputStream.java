package com.viewics.staticProxy;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class StatOutputStream extends OutputStream {
	long counter;
	OutputStream stream;
	
	public StatOutputStream(OutputStream stream) {
		this.stream = new BufferedOutputStream(stream);
		this.counter = 0l;
	}

	@Override
	public void write(int b) throws IOException {
		stream.write(b);
		counter++;
	}
	
	public long getCounter() {
		return counter;
	}
	
	public void resetCounter() {
		counter = 0;
	}

	@Override
	public void flush() throws IOException {
		stream.flush();
	}
	
	@Override
	public void close() throws IOException {
		stream.close();
	}	

}
