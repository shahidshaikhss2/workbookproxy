package com.viewics.staticProxy;

import java.io.IOException;
import java.io.InputStream;

public class StatInputStream extends InputStream {
	long counter;
	InputStream stream;
	
	public StatInputStream(InputStream stream) {
		this.stream = stream;
		this.counter = 0l;
	}

	@Override
	public int read() throws IOException {
		int data = stream.read();
		counter++;
		return data;
	}
	
	public long getCounter() {
		return counter;
	}
	
	public void resetCounter() {
		counter = 0;
	}

}
