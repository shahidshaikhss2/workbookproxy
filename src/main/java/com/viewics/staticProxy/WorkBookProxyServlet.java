package com.viewics.staticProxy;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * Proxy for Workbook Static content.
 * 
 * Caters requests starting with: /wbinfo
 * 
 *
 * Handles Content-Type: multipart/form-data; boundary=... Content-Type:
 * application/x-www-form-urlencoded "Content-Type: application/json
 */

public class WorkBookProxyServlet extends HttpServlet {

	private static final long serialVersionUID = 9086833702958667182L;
	private static final boolean DEBUG = false;
	private static final String EXPORT_COOKIE_NAME = "exportcsv", EXPORT_FAILED = "FAILED", EXPORT_SUCCESS = "SUCCESS",
			CROSS_TAB_ERROR_URL = "crosstaberror";

	@Override
	public void init(ServletConfig config) {

	}

	/*
	 * @Override protected void service(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * doHttpServiceEx(request, response); }
	 */

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doHttpServiceEx(request, response); //response.setContentType("text/html");
		
		

		StringBuffer clienturl = request.getRequestURL();
		System.out.println("\n\n*** :>" + clienturl);
		int sp2 = clienturl.indexOf("/", clienturl.indexOf("wbinfo"));
		// System.out.println(sp2);
		// System.out.println(clienturl.substring(sp2));
		clienturl.substring(sp2);
		String path = clienturl.substring(sp2 + 1);
		String extension = null;
		if (path.contains(".")) {
			extension = path.substring(path.lastIndexOf("."));
		}
		/*
		 * if (".css".equals(extension)) { response.setContentType("text/css");
		 * System.out.println("Content Type :>"+"text/css"); } else if
		 * (".js".equals(extension)) {
		 * response.setContentType("application/javascript");
		 * System.out.println("Content Type :>"+"application/javascript"); } else if
		 * (".woff2".equals(extension)) { response.setContentType("font/woff2");
		 * System.out.println("Content Type :>"+"font/woff2"); }else {
		 * response.setContentType("text/html");
		 * System.out.println("Content Type :>"+"text/html"); }
		 */

		
		String s3Bucket = "static-content-poc";
		String s3AccessKey = "AKIAVOJRX55W6WANMN7X";
		String s3SecretKey = "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";
		String s3Region = "eu-central-1";
		AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3AccessKey, s3SecretKey)))
				.withRegion(s3Region).build();

		/*
		 * ClientConfiguration clientConfiguration = new ClientConfiguration();
		 * clientConfiguration.setSignerOverride("AWSS3V4SignerType"); Protocol protocol
		 * = null; if (protocol != null) clientConfiguration.setProtocol(protocol);
		 * AmazonS3Client amazonS3 = new AmazonS3Client(new
		 * BasicAWSCredentials(s3AccessKey, s3SecretKey), clientConfiguration); if
		 * (s3Region != null && !"".equals(s3Region)) { amazonS3.setEndpoint(s3Region);
		 * }
		 */

		List<String> result = new ArrayList<String>();
		ListObjectsV2Result objList = null;
		ListObjectsV2Request request1 = new ListObjectsV2Request();
		request1.setBucketName(s3Bucket);

		request1.setPrefix(path);
		/*
		 * do { objList = amazonS3.listObjectsV2(request1); if (objList != null) {
		 * List<S3ObjectSummary> summaries = objList.getObjectSummaries(); if (summaries
		 * != null) { for (S3ObjectSummary summary : summaries) { // ignore folders if
		 * (!summary.getKey().endsWith("/")) { result.add(summary.getKey()); } } } }
		 * 
		 * request1.setContinuationToken(objList.getNextContinuationToken()); } while
		 * (objList != null && objList.isTruncated());
		 */

		// System.out.println(result);
		
		PrintWriter out = null;
		ServletOutputStream servletOut =null;
		if (".css".equals(extension) || ".js".equals(extension) || ".html".equals(extension) || ".woff2".equals(extension)) {
			// response.setContentType("application/javascript");
			out = response.getWriter();
			System.out.println("Printwriter Writing");
			String s3ObjString = amazonS3.getObjectAsString(s3Bucket, path);
			out.println(s3ObjString);
			// System.out.println("\ns3ObjString Content:>"+s3ObjString);
		} /*
			 * else if (".woff2".equals(extension)) { response.setContentType("font/woff2");
			 * System.out.println("Content Type :>" + "font/woff2"); }
			 */ else {
			//response.setContentType("text/html");
			servletOut = response.getOutputStream();
			System.out.println("servletOut Writing");
			try {
				S3Object o = amazonS3.getObject(s3Bucket, path);
				S3ObjectInputStream s3is = o.getObjectContent();
				byte[] read_buf = new byte[16384];
				int read_len = 0;
				while ((read_len = s3is.read(read_buf)) > 0) { 
					//fos.write(read_buf, 0, read_len);
					servletOut.write(read_buf);
				}

				s3is.close();
				//fos.close();
				//out.println(s3ObjString);
				//System.out.println("s3ObjString :>" + s3ObjString);
			} catch (AmazonServiceException e) {
				System.err.println("path :" + path + "\t\t" + e.getErrorMessage());
				// System.exit(1);
			}
		}
		
		
		
		//ServletOutputStream servletOut = response.getOutputStream();
		 /*
			 * catch (FileNotFoundException e) { System.err.println(e.getMessage());
			 * //System.exit(1); } catch (IOException e) {
			 * System.err.println(e.getMessage()); //System.exit(1); }
			 */
		/*
		 * System.out.println("Done!");
		 * 
		 * System.out.println( "getAsList::Location Path: " + path +
		 * " S3 Continuation Token :" + objList.getNextContinuationToken());
		 */
	}

	/*
	 * private void doHttpServiceEx(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { HttpURLConnection servercon
	 * = forwardRequest(request); forwardResponse(response, servercon, request); }
	 * 
	 * private HttpURLConnection forwardRequest(HttpServletRequest clientrequest)
	 * throws ServletException, IOException { HttpURLConnection servercon = null;
	 * String method = clientrequest.getMethod(); StringBuffer clienturl =
	 * clientrequest.getRequestURL(); StringBuffer serverurl = new StringBuffer();
	 * 
	 * System.out.println("*** :>" + clienturl); int sp2 = clienturl.indexOf("/",
	 * clienturl.indexOf("wbinfo")); System.out.println(sp2);
	 * System.out.println(clienturl.substring(sp2)); clienturl.substring(sp2);
	 * String path = clienturl.substring(sp2 + 1);
	 * 
	 * InputStream clientin = null; OutputStream serverout = null;
	 * 
	 * // debug if (DEBUG) debugHttpRequest(clientrequest);
	 * 
	 * do { if (clienturl == null) { throw new
	 * ServletException("Request URL not provided."); } // get query string (if
	 * available) String qs = clientrequest.getQueryString();
	 * 
	 * if (qs != null && qs.length() > 0) { clienturl.append("?").append(qs); } //
	 * setup target url int sp1 = clienturl.indexOf("://");
	 * 
	 * if ((sp1 != -1) && ((sp2 = clienturl.indexOf("/", sp1 + 3)) != -1)) {
	 * serverurl =
	 * serverurl.append("https://static-content-poc.s3.eu-central-1.amazonaws.com/")
	 * ; serverurl.append(path); } else { throw new
	 * ServletException("Unable to identify host from URL: " +
	 * clienturl.toString()); }
	 * 
	 * // open connection to server servercon = (HttpURLConnection) new
	 * URL(serverurl.toString()).openConnection();
	 * servercon.setInstanceFollowRedirects(false);
	 * 
	 * // set target method servercon.setRequestMethod(method); if
	 * (!"GET".equalsIgnoreCase(method)) { servercon.setDoOutput(true);
	 * servercon.setDoInput(true); servercon.setUseCaches(false); }
	 * 
	 * // copy headers over Enumeration<String> headers =
	 * clientrequest.getHeaderNames(); while (headers.hasMoreElements()) { boolean
	 * bskip = false; String header = headers.nextElement();
	 * 
	 * // do not forward certain headers (see list) for (int j = 0; j <
	 * HTTP_SERVER2CLIENT_SKIP_HEADERS_FROM_REQUEST.length; j++) { if
	 * (header.equalsIgnoreCase(HTTP_SERVER2CLIENT_SKIP_HEADERS_FROM_REQUEST[j])) {
	 * bskip = true; } }
	 * 
	 * if (bskip) { continue; }
	 * 
	 * servercon.setRequestProperty(header, clientrequest.getHeader(header)); }
	 * 
	 * servercon.setRequestProperty("Authorization",
	 * "AWS4-HMAC-SHA256 Credential=AKIAVOJRX55W6WANMN7X/20210628/eu-central-1/S3/aws4_request, SignedHeaders=host;x-amz-date, Signature=31025c1e9a514e5616b028a35c38695c84061ea643db126d92be01a935ebd3c2"
	 * ); // debug if (DEBUG) debugHttpURLConnection(servercon);
	 * 
	 * // connect to target server servercon.connect();
	 * 
	 * // transfer body if (servercon.getDoOutput()) { clientin =
	 * clientrequest.getInputStream(); serverout = servercon.getOutputStream();
	 * copyStream(clientin, serverout, -1); clientrequest.getInputStream().close();
	 * servercon.getOutputStream().close(); }
	 * 
	 * } while (false);
	 * 
	 * return servercon; }
	 */

	/*
	 * private static final String[] HTTP_SERVER2CLIENT_SKIP_HEADERS = new String[]
	 * { "Connection", "Server", "Transfer-Encoding", "Content-Length" };
	 * 
	 * private static final String[] HTTP_SERVER2CLIENT_SKIP_HEADERS_FROM_REQUEST =
	 * new String[] { "X-Forwarded-Host", "X-Host", "X-Forwarded-Server" };
	 * 
	 * private void forwardResponse(HttpServletResponse response, HttpURLConnection
	 * servercon, HttpServletRequest request) throws ServletException, IOException {
	 * 
	 * int serverrc = 0; int ctntlen = -1; InputStream serverin = null; OutputStream
	 * clientout = null;
	 * 
	 * try { // set status of http response serverrc = servercon.getResponseCode();
	 * response.setStatus(serverrc);
	 * 
	 * logger.debug("DEBUG RESPONSE [" + serverrc + "] From " +
	 * servercon.getURL().toString());
	 * 
	 * // copy http headers from server to client response int maxHeader =
	 * servercon.getHeaderFields().size(); for (int i = 1; i < maxHeader + 1; ++i) {
	 * String header = servercon.getHeaderFieldKey(i); String value =
	 * servercon.getHeaderField(i);
	 * 
	 * boolean bskip = false; if (header == null) continue; // get content-length
	 * (if available) if (header.equalsIgnoreCase("content-length")) { ctntlen =
	 * Integer.parseInt(servercon.getHeaderField(i)); continue; }
	 * 
	 * // do not forward certain headers (see list) for (int j = 0; j <
	 * HTTP_SERVER2CLIENT_SKIP_HEADERS.length; j++) { if
	 * (header.equalsIgnoreCase(HTTP_SERVER2CLIENT_SKIP_HEADERS[j])) { bskip = true;
	 * } }
	 * 
	 * if (bskip) continue;
	 * 
	 * if (value != null && !value.isEmpty()) { String baseURL =
	 * applicationconfiguration.getBaseURL(); if
	 * (header.equalsIgnoreCase("location")) {
	 * 
	 * UriComponents valBuilder =
	 * ServletUriComponentsBuilder.fromUriString(value).build();
	 * 
	 * StringBuilder builder = new StringBuilder(baseURL);
	 * builder.append(valBuilder.getPath());
	 * 
	 * UriComponentsBuilder finalURL =
	 * UriComponentsBuilder.fromHttpUrl(builder.toString());
	 * finalURL.queryParams(valBuilder.getQueryParams());
	 * 
	 * value = finalURL.build().toString(); }
	 * 
	 * if (value.contains("Secure;") && baseURL.startsWith("http:")) { String[]
	 * split = value.split("Secure;"); value = split[0] + split[1]; } }
	 * 
	 * response.addHeader(header, value); }
	 * 
	 * // copy http response stream to client response try { serverin =
	 * servercon.getInputStream(); } catch (Exception e) { logger.error("", e);
	 * serverin = servercon.getErrorStream(); } clientout =
	 * response.getOutputStream(); String url = servercon.getURL().toString();
	 * 
	 * // copyStream(serverin, clientout, ctntlen); if
	 * (((url.contains("exportcrosstab") || url.contains("tempfile")) &&
	 * url.contains("download=true")) || url.contains(CROSS_TAB_ERROR_URL)) {
	 * boolean otherRequest = false; UriComponents valBuilder =
	 * ServletUriComponentsBuilder.fromUriString(url).build(); if
	 * (valBuilder.getQueryParams().get("wbname") != null &&
	 * valBuilder.getQueryParams().get("sheetname") != null) { String workbookName =
	 * valBuilder.getQueryParams().get("wbname").get(0); String path =
	 * applicationconfiguration.getTempDirectory() + workbookName; File f = new
	 * File(path); if (f.isDirectory() && !f.exists()) { f.mkdirs(); } String
	 * fileName =
	 * URLDecoder.decode(valBuilder.getQueryParams().get("sheetname").get(0));
	 * 
	 * // Check if all sheets are failed String exportCookie = getCookie(request,
	 * EXPORT_COOKIE_NAME); if (exportCookie == null ||
	 * !exportCookie.equalsIgnoreCase(EXPORT_FAILED)) { // If Export sheet //
	 * successful, then // only create file // in folder if
	 * (url.contains(CROSS_TAB_ERROR_URL)) { // Error in download crosstab because
	 * of no data File sheet = new File(f + "/" +
	 * FileUtil.cleanUpFileNameForS3Storage(fileName) + ".csv");
	 * createEmptyFile(sheet); } else { File sheet = new File(f + "/" +
	 * FileUtil.cleanUpFileNameForS3Storage(fileName) + ".csv");
	 * FileUtils.copyInputStreamToFile(serverin, sheet); while (ctntlen !=
	 * FileUtils.sizeOf(sheet)) { // till all data copied to file } } Cookie cookie
	 * = new Cookie(EXPORT_COOKIE_NAME, EXPORT_SUCCESS); response.addCookie(cookie);
	 * } else { // Reset cookie Cookie cookie = new Cookie(EXPORT_COOKIE_NAME,
	 * EXPORT_SUCCESS); response.addCookie(cookie);
	 * 
	 * logger.info("Could not download {} sheet from workbook {} in csv export",
	 * fileName, workbookName); }
	 * 
	 * Integer totalSheets =
	 * Integer.valueOf(valBuilder.getQueryParams().get("totalSheets").get(0));
	 * Integer index =
	 * Integer.valueOf(valBuilder.getQueryParams().get("index").get(0)); if (index +
	 * 1 == totalSheets) { // Reset cookie Cookie cookie = new
	 * Cookie(EXPORT_COOKIE_NAME, EXPORT_SUCCESS); response.addCookie(cookie);
	 * createZipFile(path); } } else if (serverrc != 200) { Cookie cookie = new
	 * Cookie(EXPORT_COOKIE_NAME, EXPORT_FAILED); response.addCookie(cookie);
	 * response.setStatus(200);
	 * logger.info("Could not download sheet in csv export"); } else { otherRequest
	 * = true; // This is to handle image/pdf download support for old tabelau }
	 * 
	 * clientout = response.getOutputStream();
	 * 
	 * if (otherRequest) { copyStream(serverin, clientout, ctntlen); } } else {
	 * clientout = response.getOutputStream(); copyStream(serverin, clientout,
	 * ctntlen); } if (DEBUG) debugHttpResponse(response); } finally { if (serverin
	 * != null) serverin.close(); if (clientout != null) clientout.close(); } }
	 * 
	 * private String getCookie(HttpServletRequest request, String cookieName) {
	 * Cookie[] cookies = request.getCookies(); if (cookies != null) { for (Cookie
	 * cookie : cookies) { if (cookie.getName().equals(cookieName)) { return
	 * cookie.getValue(); } } } return null; }
	 * 
	 * private void createZipFile(String sourceFolderPath) { // Check if any file is
	 * created File f = new File(sourceFolderPath); if (!f.exists()) { f.mkdirs();
	 * 
	 * // If no files are created, because export csv is failed for all sheets,
	 * create // empty csv with error message if (f.listFiles().length == 0) { File
	 * sheet = new File(f + "/" + FileUtil.cleanUpFileNameForS3Storage(f.getName())
	 * + ".csv"); createEmptyFile(sheet); } }
	 * 
	 * String zipName = sourceFolderPath + ".zip"; try {
	 * fileUtil.createZipFile(sourceFolderPath, zipName); } catch (IOException e) {
	 * logger.error("Error while creating zip for file : {} {}", zipName, e); } }
	 * 
	 * private void createEmptyFile(File sheet) { InputStream inputStream = null;
	 * try { String message = "No data available."; inputStream = new
	 * ByteArrayInputStream(message.getBytes());
	 * FileUtils.copyInputStreamToFile(inputStream, sheet); } catch (Exception e) {
	 * logger.error("Error while creating empty file", e); } finally { if
	 * (inputStream != null) { try { inputStream.close(); } catch (IOException e) {
	 * logger.error("Ignoring error...", e); } } } }
	 * 
	 * private void copyStream(InputStream in, OutputStream out, int length) throws
	 * IOException { byte[] obuffer = new byte[16384]; int obtoread =
	 * obuffer.length; int ocount = 0; int ototal = 0;
	 * 
	 * if (in == null || out == null) return;
	 * 
	 * do { if (length != -1) obtoread = length - ototal > obuffer.length ?
	 * obuffer.length : length - ototal; if (obtoread == 0) break;
	 * 
	 * ocount = in.read(obuffer, 0, obtoread); if (ocount < 0) break; ototal +=
	 * ocount;
	 * 
	 * out.write(obuffer, 0, ocount); } while (true); }
	 * 
	 * private static final String INDENT = "\t"; private static final String LF =
	 * "\n";
	 * 
	 * private void debugHttpRequest(HttpServletRequest request) { StringBuffer sb =
	 * new StringBuffer();
	 * sb.append("Debug Client Request: ").append(request.getRequestURI()).append(LF
	 * ); sb.append(INDENT).append("[RequestURI] ").append(request.getRequestURI()).
	 * append(LF);
	 * sb.append(INDENT).append("[Method] ").append(request.getMethod()).append(LF);
	 * sb.append(INDENT).append("[QueryString] ").append(request.getQueryString()).
	 * append(LF);
	 * sb.append(INDENT).append("[PathInfo] ").append(request.getPathInfo()).append(
	 * LF);
	 * sb.append(INDENT).append("[ContentType] ").append(request.getContentType()).
	 * append(LF); Enumeration<String> hNames = request.getHeaderNames(); while
	 * (hNames.hasMoreElements()) { String name = hNames.nextElement();
	 * sb.append(INDENT).append("[header] ").append(name).append("=").append(request
	 * .getHeader(name)).append(LF); if (name.equalsIgnoreCase("Cookie")) {
	 * sb.append(INDENT).append("[Cookie] ").append(getCookie(name)).append(LF); } }
	 * 
	 * logger.debug(sb.toString()); }
	 * 
	 * private void debugHttpResponse(HttpServletResponse response) { StringBuffer
	 * sb = new StringBuffer(); sb.append("Debug Client response: ");
	 * Collection<String> headerNames = response.getHeaderNames(); for (String name
	 * : headerNames) {
	 * sb.append(INDENT).append("[header] ").append(name).append("=").append(
	 * response.getHeader(name)).append(LF); if (name.equalsIgnoreCase("Cookie")) {
	 * sb.append(INDENT).append("[Cookie] ").append(getCookie(name)).append(LF); } }
	 * 
	 * logger.debug(sb.toString()); }
	 * 
	 * protected String getCookie(String cookieValue) { StringBuilder escapedCookie
	 * = new StringBuilder(); String cookies[] = cookieValue.split("[;,]"); for
	 * (String cookie : cookies) { String cookieSplit[] = cookie.split("="); if
	 * (cookieSplit.length == 2) { String cookieName = cookieSplit[0].trim();
	 * escapedCookie.append(cookieName).append("=").append(cookieSplit[1].trim()); }
	 * } return escapedCookie.toString(); }
	 * 
	 * private void debugHttpURLConnection(HttpURLConnection con) { StringBuffer sb
	 * = new StringBuffer();
	 * sb.append("Debug Server Request: ").append(con.getURL().toString()).append(LF
	 * );
	 * sb.append(INDENT).append("[URL] ").append(con.getURL().toString()).append(LF)
	 * ;
	 * sb.append(INDENT).append("[Method] ").append(con.getRequestMethod()).append(
	 * LF);
	 * 
	 * Map<String, List<String>> propsmap = con.getRequestProperties(); Set<String>
	 * keys = propsmap.keySet(); for (String key : keys) { List<String> values =
	 * propsmap.get(key); if (values == null) continue; for (String value : values)
	 * {
	 * sb.append(INDENT).append("[header] ").append(key).append("=").append(value).
	 * append(LF); } } logger.debug(sb.toString()); }
	 */

	public static void main(String[] args) {
		/*
		 * String location =
		 * "https://ts-eu.viewics.com/t/demosite/views/en_null_688/DashboardPerformance/en_ViewicsPulse2/viewics_11_615_2230_1526372084813"
		 * +
		 * "?_UserEmail=viewicsadmin@viewics.com&_UserID=1&_TenantId=1&_TenantAlias=viewics&_TenantName=Viewics&IsExec=No&:embed=y&:showVizHome=n&:tabs=n&:toolbar=n&:apiID=host0";
		 * String request =
		 * "https://localhost:8080/trusted/e50ql8NvT6qt5gKYoy2jhA==:4iVBWUM9RBvJbdqluoynJgbf/t/pulsedevmaj/views/en_acme_DW_4570/DashboardHierarchyWise?_UserEmail=lisa.sullivan@acme.com&_UserID=5&_TenantId=2&_TenantAlias=acme&_TenantName=ACME&Tenant_tag=asd&:original_view=yes&client_ip=0:0:0:0:0:0:0:1&:embed=y&:showVizHome=n&:tabs=n&:toolbar=n&:apiID=host0";
		 * 
		 * UriComponentsBuilder fromUriString =
		 * ServletUriComponentsBuilder.fromUriString(request); UriComponents build =
		 * fromUriString.build();
		 * 
		 * UriComponentsBuilder valOfLoation =
		 * ServletUriComponentsBuilder.fromUriString(location); UriComponents valBuilder
		 * = valOfLoation.build();
		 * 
		 * StringBuilder builder = new StringBuilder();
		 * builder.append(build.getScheme()); builder.append("://");
		 * builder.append(build.getHost()); builder.append(":");
		 * builder.append(build.getPort()); builder.append(valBuilder.getPath());
		 * 
		 * System.out.println(builder.toString());
		 */}
}
